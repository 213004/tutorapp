export default {
  async fetchAllChats(context) {
    const userId = context.rootGetters.userId;
    const token = context.rootGetters.token;

    const response = await fetch(
      `https://tutorapp-5e917-default-rtdb.europe-west1.firebasedatabase.app/chats.json?auth=` + token
    );
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch chats.');
      throw error;
    }

    const chats = {};

    for (const key in responseData) {
      const chat = responseData[key];
      if (chat.studentId === userId || chat.tutorId === userId) {
        chats[key] = chat;
      }
    }

    context.commit('setChats', chats);
  },

  async createChat(context, payload) {
    const newChat = {
      studentId: context.rootGetters.userId,
      studentHandle: context.rootGetters.userHandle,
      tutorId: payload.tutorId,
      tutorHandle: payload.tutorHandle,
      lastMessageTimestamp: Date.now()
    };

    const chatId = newChat.studentId + '_' + newChat.tutorId;
    const token = context.rootGetters.token;

    const response = await fetch(
      `https://tutorapp-5e917-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}.json?auth=` + token,
      {
        method: 'PUT',
        body: JSON.stringify(newChat),
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to create new chat.');
      throw error;
    }

  },

  async sendMessage(context, payload) {
    const newMessage = {
      text: payload.text,
      timestamp: payload.timestamp,
      senderId: context.rootGetters.userId,
    };

    const chatId = payload.chatId;
    const token = context.rootGetters.token;

    const chatResponse = await fetch(
      `https://tutorapp-5e917-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}.json?auth=` + token
    );
    const chatData = await chatResponse.json();

    if (!chatResponse.ok) {
      const error = new Error(chatData.message || 'Failed to fetch chat.');
      throw error;
    }

      const messageId = payload.messageId;

      const response = await fetch(
        `https://tutorapp-5e917-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}/messages/${messageId}.json?auth=` + token,
        {
          method: 'PUT',
          body: JSON.stringify(newMessage),
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
      const responseData = await response.json();

      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to add new message.');
        throw error;
      }

      const response2 = await fetch(
        `https://tutorapp-5e917-default-rtdb.europe-west1.firebasedatabase.app/chats/${chatId}/lastMessageTimestamp.json?auth=` + token,
        {
          method: 'PUT',
          body: JSON.stringify(payload.timestamp),
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
      const responseData2 = await response2.json();

      if (!response2.ok) {
        const error = new Error(responseData2.message || 'Failed to update lastMessageTimestamp.');
        throw error;
      }

  },
};
  
export default {
    chats(state) {
        return state.chats;
    },
    hasChats(state) {
        return Object.keys(state.chats).length > 0;
    },
    chatById: (state) => (chatId) => {
        return state.chats[chatId];
    },
    chatsForUser: (state) => (userId) => {
        const chats = [];
        for (const chatId in state.chats) {
            const chat = state.chats[chatId];
            if (chat.studentId === userId || chat.tutorId === userId) {
                chats.push({ id: chatId, ...chat });
            }
        }
        return chats;
    }
};
  
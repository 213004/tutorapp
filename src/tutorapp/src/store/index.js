import { createStore } from 'vuex';

import tutorsModule from './modules/tutors/index.js'
import chatsModule from './modules/chats/index.js'
import authModule from './modules/auth/index.js'

const store = createStore({
    modules: {
        tutors: tutorsModule,
        chats: chatsModule,
        auth: authModule
    }
});

export default store;
import { createRouter, createWebHistory } from "vue-router";

import TutorsList from "./pages/tutors/TutorsList.vue";
import ChatsList from "./pages/chats/ChatsList.vue";
import NotFound from "./pages/NotFound.vue";
import store from "./store/index.js";

const TutorDetails = () => import('./pages/tutors/TutorDetails.vue');
const ChatDetails = () => import('./pages/chats/ChatDetails.vue');
const ChatInput = () => import('./components/chats/ChatInput.vue');
const UserAuth = () => import('./pages/auth/UserAuth.vue');
const TutorRegistration = () => import('./pages/tutors/TutorRegistration.vue');

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/tutors' },
        { path: '/tutors', component: TutorsList },
        { path: '/tutors/:id', component: TutorDetails, props: true },
        { path: '/register', component: TutorRegistration, meta: { requiresAuth: true } },
        { path: '/chats', component: ChatsList, meta: { requiresAuth: true } },
        { 
            path: '/chats/:id',
            component: ChatDetails,
            props: true,
            children: [
                { path: 'chatInput', component: ChatInput }
            ]
        },
        { path: '/auth', component: UserAuth, meta: { requiresUnauth: true } },
        { path: '/:notFound(.*)', component: NotFound }
    ]
});

router.beforeEach(function(to, _, next) {
    if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
        next('/auth');
    }
    else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
        next('/tutors');
    }
    else {
        next();
    }
});

export default router;